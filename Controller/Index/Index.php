<?php

namespace Adithyan\Sample\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\Page;

class Index extends Action
{
    /**
     * @param Context $context
     */
    public function __construct(
        Context             $context,
    )
    {
        parent::__construct($context);
    }


    /**
     * Default customer account page
     *
     */
    public function execute()
    {
        exit('testing');
    }
}
